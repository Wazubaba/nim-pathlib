import strutils

proc sanitize*(path: string): string =
  result = "'"
  result &= path.replace("'", "'\\''")#[
  result = path.multiReplace([
    ("#", "\\#"), ("!", "\\!"), ("\"", "\\\""),
    ("$", "\\$"), ("&", "\\&"), ("'", "\\'"),
    ("(", "\\("), (")", "\\)"), (" ", "\\ "),
    ("*", "\\*"), (",", "\\,"), (":", "\\:"),
    (";", "\\;"), ("<", "\\<"), ("=", "\\="),
    (">", "\\>"), ("?", "\\?"), ("@", "\\@"),
    ("[", "\\["), ("\\", "\\\\"), ("]", "\\]"),
    ("^", "\\^"), ("`", "\\`"), ("{", "\\{"),
    ("|", "\\|"), ("}", "\\}")])]#
  result &= "'"

