# Package

version       = "0.1.2"
author        = "Wazubaba"
description   = "Simple utilities to make working with paths easier"
license       = "LGPL-3.0"
srcDir        = "src"



# Dependencies

requires "nim >= 0.20.0"
